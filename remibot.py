#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import re

import xmpp
from remidb import RemiDB
from remiparser import RemiParser
import remihelp

jid = "japybot@jabber.org"
password = "japybot"


class RemiBot:
    def __init__(self, jid, password):
        self.uhelp = remihelp.main_help
        self.TIMESTAMP = 0
        # Init xmpp connection
        """Connecting to xmpp"""
        print "1. Connecting to xmpp..."
        self.jid = xmpp.JID(jid)
        self.bot = xmpp.Client(self.jid.getDomain(), debug=[])
        con = self.bot.connect()
        if not con:
            print "Can't connect."
        auth = self.bot.auth(self.jid.getNode(), password)
        if not auth:
            print "Can't authenticate."
        self.bot.RegisterHandler('message', self.bot_message)
        self.bot.RegisterHandler('presence', self.bot_presence)
        self.bot.sendInitPresence()
        print "    Connected!"
        # Create db obj
        self.dbase = RemiDB()
        # Create parser
        self.parser = RemiParser()

    def mainloop(self):
        try:
            while True:
                self.bot.Process(1)
                self.time_check()
            self.bot.disconnect()
        except KeyboardInterrupt:
            self.bot.disconnect()
            sys.exit(1)

    def bot_message(self, conn, msg):
        user = str(msg.getFrom()).split('/')[0]
        text = msg.getBody()
        if text:
            if re.match("^help$", text.strip().lower()):
                self.bot.send(xmpp.Message(user, self.uhelp))
            elif re.match("^help[\ ]+format$", text.strip().lower()):
                self.bot.send(xmpp.Message(user, remihelp.format_help))
            elif re.match("^help[\ ]+tz$", text.strip().lower()):
                self.bot.send(xmpp.Message(user, remihelp.tz_help))
            elif re.match("^tz[\ ]+(\+|\-)\d+", text.strip().lower()):
                tz = re.findall("[\+|\-]\d+", text.strip().lower())
                self.dbase.tz_add(user, tz)
                # For test:
                self.bot.send(xmpp.Message(user, tz))
            elif re.match("^list$", text.strip().lower()):
                ulist = self.dbase.list_all(user)
                for row in ulist:
                    msg = str(row[0]) + " " + row[3]
                    self.bot.send(xmpp.Message(row[2], msg))
            elif re.match("^del[\ ]+[0-9]+", text.strip().lower()):
                del_id = int(re.findall("[0-9]+",text)[0])
                self.dbase.del_id(del_id)
                self.bot.send(xmpp.Message(user, "You del some rec"))
            else:
                if self.parser.maybe_time(text, self.dbase.tz_get(user)):
                    self.dbase.add(self.parser.r_time, user, self.parser.r_text)
                    #self.bot.send(xmpp.Message(user, "This is a TIME!" + str(self.parser.r_time)))
                    if self.parser.r_time < self.TIMESTAMP:
                        self.TIMESTAMP = self.parser.r_time
                else:
                    self.bot.send(xmpp.Message(user, "Ooops"))

    def bot_presence(self, conn, node):
        if node.getType() == 'subscribe':
            self.bot.getRoster().Authorize(node.getFrom())
            self.bot.getRoster().Subscribe(node.getFrom())

    def time_check(self):
        if int(time.time()) >= self.TIMESTAMP:
            print self.TIMESTAMP
            for row in self.dbase.get_main(self.TIMESTAMP):
                self.bot.send(xmpp.Message(row[2], row[3]))
            # Delete from dbase old records
            self.dbase.del_old(self.TIMESTAMP)
            # Set new TIMESTAMP
            if self.dbase.get_timestamp():
                self.TIMESTAMP = self.dbase.tstamp

if __name__ == "__main__":
    bot = RemiBot(jid, password)
    bot.mainloop()
