#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import re
from calendar import timegm

class RemiParser():
    def __init__(self):
        self.current = time.time()
        self.r_time = 0
        self.r_text = "Some message"
        pass

    def maybe_time(self, text, tz):
        self.current = time.time()
        """"Check string, maybe it is time, if so return it"""
        if self.parse_time_1(text):
            return 1
        elif self.parse_time_2(text, tz):
            return 1
        else:
            return 0

    def parse_time_1(self, text):
        """ Parser for time in this format
                4d 3h 15m msg
        """
        regex = "(?:(\d*\s*d)?\s*(\d*\s*h)?\s*(\d*\s*m)?\s)?(.*)"
        #regex = "(\d*\s*d)?\s*(\d*\s*h)?\s*(\d*\s*m )?(.*)"
        result = re.match(regex, text + " ").groups()
        print result
        da = 0
        ho = 0
        mi = 0
        if result[0]:
            tmp = re.findall("\d+", result[0])
            try:
                da = int(tmp[0])
            except:
                da = 1
        if result[1]:
            tmp = re.findall("\d+", result[1])
            try:
                ho = int(tmp[0])
            except:
                ho = 1
        if result[2]:
            tmp = re.findall("\d+", result[2])
            try:
                mi = int(tmp[0])
            except:
                mi = 1
        if result[-1] != result[2]:
            self.r_text = result[-1]
        nt = int(time.time()) + (86400 * da) + (3600 * ho) + (60 * mi)
        if self.current < nt:
            self.r_time = nt
            return 1
        else:
            return 0

    def parse_time_2(self, text, tz = "-3"):
        if not tz:
            tz = "-3"
        regex = "(?:((?:19|20|)\d\d)[-/.](\d{1,2})[-/.](\d{1,2})\s+)?(?:(\d{1,2}):(\d{1,2})\s+)?(.*)"
        result = re.match(regex, text + " ").groups()
        tzre = re.match("(\+|\-)(\d+)", tz).groups()
        if tzre[0] == "-":
            tz_flag = 1
        else:
            tz_flag = 0
        tz = int(tz[1])
        print result
        ye = 0
        mo = 0
        da = 0
        ho = 0
        mi = 0
        # Years
        if result[0]:
            ye = int(result[0])
        else:
            ye = time.gmtime()[0]
        # Mon
        if result[1]:
            mo = int(result[1])
        else:
            mo = time.gmtime()[1]
        # Days
        if result[2]:
            da = int(result[2])
        else:
            da = time.gmtime()[2]
        # Hours
        if result[3]:
            ho = int(result[3])
        else:
            ho = time.gmtime()[3]
        # Minutes
        if result[4]:
            mi = int(result[4])
        else:
            mi = time.gmtime()[4]
        t_time = (ye, mo, da, ho, mi, 0)
        print t_time
        if result[-1] != result[4]:
            self.r_text = result[-1]
        # Create time
        if tz_flag:
            nt = timegm(t_time) + 3600*tz
        else:
            nt = timegm(t_time) - 3600*tz
        print nt
        if self.current < nt:
            self.r_time = nt
            return 1
        else:
            return 0

if __name__ == "__main__":
    test = RemiParser()
    test.parse_time_2("2012-05-30 10:45")
    t_t = test.r_time
    n_t = time.time()
    print "-"*10
    print t_t, " ---- ", n_t
    print time.localtime(t_t)
    print time.localtime(n_t)
    print test.r_text
