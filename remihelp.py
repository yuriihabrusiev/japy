main_help = """To set the time when I'll send you a message use `datetime message_text` (`message_text` is optional). To see `datetime` format use `help format`.
To see records list use `list`.
To delete record use `del record_number`.
To set timezone use `tz timezone_name`. To see timezones help use `help tz`. Current timezone `tz`
"""

format_help = """Datetime formats.
`7d 24h 60m`
`year-month-day hour:minute`
"""

tz_help =  """Set up tz.
Example: `tz +3` will set tz to Kiev
"""
