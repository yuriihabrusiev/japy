#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

class RemiDB:
    def __init__(self):
        self.dbName = "test.db"
        self.tstamp = 0
        """Connecting to database"""
        print "2. Connecting to dbase..."
        self.con = sqlite3.connect(self.dbName)
        self.cur = self.con.cursor()
        try:
            self.cur.execute("SELECT * \
                              FROM main \
                              ORDER BY time \
                              LIMIT 1")
        except:
            self.cur.execute("CREATE TABLE main ( \
                              id INTEGER PRIMARY KEY, \
                              time INTEGER, \
                              user VARCHAR, \
                              msg VARCHAR)")
            self.cur.execute("CREATE TABLE tz ( \
                              id INTEGER PRIMARY KEY, \
                              user VARCHAR, \
                              utz VARCHAR)")
            self.con.commit()
        print "    Connected!"

    def add(self, utime, user, utext):
        '''Add to dbase time, username and msg'''
        utime = str(utime)
        self.cur.execute("INSERT \
                          INTO main(time, user, msg) \
                          VALUES (?,?,?)",
                          (utime, user, utext))
        self.con.commit()

    def del_id(self, uid):
        '''Delete by id'''
        self.cur.execute("DELETE FROM main \
                          WHERE id == ?",
                        (uid,))
        self.con.commit()

    def del_old(self, time):
        '''Delete older than time'''
        self.cur.execute("DELETE FROM main \
                          WHERE time <= ?",
                         (time,))
        self.con.commit()

    def list_all(self, user):
        '''Return all user msgs'''
        self.cur.execute("SELECT * \
                          FROM main \
                          WHERE user == ?",
                        (user,))
        ulist = self.cur.fetchall()
        #if ulist:
        return ulist
            #for row in ulist:
            #    msg = str(row[0]) + " " + row[3]
                #self.bot.send(xmpp.Message(row[2], msg))

    def get_main(self, time):
        self.cur.execute("SELECT * \
                          FROM main \
                          WHERE time <= ?",
                         (time,))
        return self.cur.fetchall()

    def get_timestamp(self):
        self.cur.execute("SELECT * \
                          FROM main \
                          ORDER BY time")
        base = self.cur.fetchall()
        if base:
            self.tstamp = base[0][1]
            return 1
        else:
            return 0

    def tz_add(self, user, tz):
        self.cur.execute("INSERT \
                          INTO tz(user, utz) \
                          VALUES (?,?)",
                          (user, tz))
        self.con.commit()

    def tz_get(self, user):
        self.cur.execute("SELECT * \
                          FROM tz \
                          WHERE user == ? \
                          LIMIT 1",
                        (user,))
        utz = self.cur.fetchall()
        if utz:
            return utz[0][2]
        return None

if __name__ == "__main__":
    test = RemiDB()
    #test.tz_add("h.yurka@gmail.com", "+3")
    print test.tz_get("h.yurka@gmail.com")
